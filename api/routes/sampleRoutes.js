/*jslint node: true, es5: true */

(function () {

    "use strict";

    var Fruit = require('../models/fruit.js').model;

    exports.addRoutes = function (app) {

        //Sample collection
        app.route('/fruits')
            .get(function (req, res) {
                //Get -> Get all fruits
                Fruit.find({}, function (err, data) {
                    if (err) {
                        res.json('Failed to read fruits.');
                    } else {
                        res.json(data);
                    }

                });
            })
            .post(function (req, res) {
                //Post -> Create a single fruit
                var newFruit = new Fruit({
                    name: req.body.name,
                    features : {
                        type : req.body.features.type,
                        color : req.body.features.color
                    }
                });

                newFruit.save(function (err) {
                    if (err) {
                        res.json('Failed to create fruit.');
                    } else {
                        res.json('New fruit created.');
                    }
                });
            });

        //Specific Fruit
        app.route('/fruits/:id')
            .get(function (req, res) {
                //Get -> Get specific fruit
                Fruit.findById(req.params.id, function (err, data) {
                    if (err) {
                        res.json('Failed to read fruit.');
                    } else {
                        res.json(data);
                    }
                });
            })
            .delete(function (req, res) {
                //Delete -> Delete specific fruit
                Fruit.findByIdAndRemove(req.params.id, function (err) {
                    if (err) {
                        res.json('Failed to remove fruit.');
                    } else {
                        res.json('Fruit removed.');
                    }
                });

            })
            .put(function (req, res) {
                //Put -> Admin Authorization
                Fruit.findByIdAndUpdate(req.params.id, {
                    $set: req.body
                }, function (err) {
                    if (err) {
                        res.json('Failed to update fruit.');
                    } else {
                        res.json('Fruit updated.');
                    }
                });
            });

    };
}());
