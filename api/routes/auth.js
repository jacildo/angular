/*jslint node: true */

(function () {

    "use strict";

    var passport = require('passport'),
        Account = require('../models/account.js'),
        AccountModel = require('../models/account.js').model;

    exports.addRoutes = function (app) {

        //User registration
        app.route('/register')
            .post(function (req, res) {
                //Post -> register a new account
//                var credentials = {
//                    username: req.body.username,
//                    password: req.body.password
//                };

                Account.register(new Account({ username : req.body.username }), req.body.password, function (err, account) {
                    if (err) {
                        res.redirect('/');
                    }

                    passport.authenticate('local')(req, res, function () {
                        res.redirect('/');
                    });
                });
            });

        //User login
        app.route('/login')
            .get(function (req, res) {
                if (req.user) {
                    res.json(user);
                } else {
                    console.log('not logged in');
                }
            })
            .post(function (req, res, next) {
                passport.authenticate('local', { failureFlash: 'Invalid username or password.', successFlash: 'Welcome!'}, function (err, user, info) {
                    if (err) {
                        console.log(err);
                        return next(err);
                    }
                    if (!user) {
                        console.log(user);
                        return res.redirect('/login');
                    }
                    req.logIn(user, function (err) {
                        if (err) {
                            console.log(err);
                            return next(err);
                        }
//                        console.log(user);
//                        return res.redirect('/');
                        res.sendStatus(200);
                    });
                })(req, res, next);
            });

        //User logout
        app.route('/logout')
            .get(function (req, res) {
                req.logout();
                res.sendStatus(200);
            });

        //REMOVE AFTER DEPLOYING
        app.route('/accounts')
            .get(function (req, res) {
                Account.find({}, function (err, data) {
                    if (err) {
                        res.json('Failed to read accounts.');
                    } else {
                        res.json(data);
                    }
                });
            })
            .delete(function (req, res) {
                Account.remove(req.body.ID, function (err) {
                    if (err) {
                        res.json("ERROR DELETING ACCOUNTS");
                    } else {
                        res.json("ACCOUNTS REMOVED");
                    }
                });
            });

    };
}());
