(function () {
    'use strict';

    // grab the nerd model we just created
    var Nerd = require('./models/nerd'),
        path           = require('path');

    module.exports = function (app) {

        // server routes ===========================================================
        // handle things like api calls
        // authentication routes

        // import routes
        require('./routes/nerd')(app);

        // route to handle creating goes here (app.post)
        // route to handle delete goes here (app.delete)

        // frontend routes =========================================================
        // route to handle all angular requests
        app.get('*', function (req, res) {
            res.sendFile('index.html', {root: path.join(__dirname, '../')}, function (err) {
                if (err) {
                    console.log(err);
                }
            }); // load our public/index.html file
        });

    };
}());

