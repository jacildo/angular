(function () {
    'use strict';

    var mongoose = require('mongoose'),
        url      = 'mongodb://127.0.0.1:27017/angular-app',
        db;

    module.exports = function () {
        mongoose.connect(url);

        db = mongoose.connection;

        db.on('error', console.error.bind(console, 'Connection error: '));
        db.once('open', function () {
            console.log('\nDatabase connection open' +
                       '\n  host: ' + db.host +
                       '\n  port: ' + db.port +
                       '\n  db:   ' + db.name
                       );
        });

    };

}());
