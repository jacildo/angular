/*jslint node: true */

(function () {

    "use strict";

    var mongoose = require('mongoose'),
        ObjectId = mongoose.Schema.Types.ObjectId,
        fruitSchema = new mongoose.Schema({
            features: {
                color: {
                    type: String,
                    required: true
                },
                type: {
                    type: String,
                    required: true
                }
            },
            name: {
                type: String,
                required: true
            }
        });

    exports.schema = fruitSchema;
    exports.model = mongoose.model('Fruit', fruitSchema);

}());
