/*jslint node: true */

(function () {

    "use strict";

    var mongoose = require('mongoose'),
        Schema = mongoose.Schema,
        bcrypt = require('bcrypt-nodejs'),
        passportLocalMongoose = require('passport-local-mongoose'),
        Account = new mongoose.Schema({
            username: String,
            password: String,
            nickname: String,
            birthdate: Date
        });

    Account.plugin(passportLocalMongoose);

    module.exports = mongoose.model('Account', Account);

}());
