(function () {
    "use strict";

    // Include gulp
    var gulp = require('gulp'),
        jshint = require('gulp-jshint'),
        sass = require('gulp-sass'),
        concat = require('gulp-concat'),
        connect = require('gulp-connect'),
        insert = require('gulp-insert'),
        util = require('gulp-util'),
        uglify = require('gulp-uglify'),
        rename = require('gulp-rename'),
        nodemon = require('gulp-nodemon'),
        webserver = require('gulp-webserver');

    // app ddependencies
    var dependencies = {
        './node_modules/angular/angular*.js': 'app/lib/angular',
        './node_modules/angular-resource/angular-resource*.js': 'app/lib/angular-resource',
        './node_modules/angular-route/angular-route*.js': 'app/lib/angular-route',
        './bower_components/jquery/dist/jquery.min.js': 'app/lib/jquery',
        './bower_components/bootstrap/dist/js/bootstrap.min.js': 'app/lib/bootstrap',
        './node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js': 'app/lib/angular-ui-bootstrap',
        './node_modules/bootstrap/dist/css/bootstrap.min.css': 'app/lib/bootstrap',
//        './node_modules/bootstrap/dist/css/bootstrap.min.css.map': 'app/lib/bootstrap',
    }

    // Lint Task
    gulp.task('lint', function () {
        return gulp.src('_Scripts/*.js')
            .pipe(jshint())
            .pipe(jshint.reporter('default'));
    });

    // Compile Our Sass
    gulp.task('sass', function () {
        return gulp.src('_Styles/*.scss')
            .pipe(sass())
            .pipe(gulp.dest('app/resources/css'));
    });

    // Concatenate & Minify JS
    gulp.task('scripts', function () {
        return gulp.src('_Scripts/*.js')
            .pipe(concat('all.js'))
            .pipe(gulp.dest('app/assets/js'))
            .pipe(rename('all.min.js'))
            .pipe(uglify())
            .pipe(gulp.dest('dist'));
    });

    // task to compile all modules into a single file named app.js
    gulp.task('app:compile', function () {
        return gulp.src(['app/appmodule.js', 'app/**/*.component.js', 'app/**/*.service.js'])
            .pipe(concat('app.js'))
            .pipe(uglify({mangle: false}).on('error', util.log))
            .pipe(gulp.dest('app'));
    });

    // copy listed dependencies into library folder
    gulp.task("app:dependencies", function () {
        for (var src in dependencies) {
            if (!dependencies.hasOwnProperty(src)) continue;
            gulp.src(src)
                .pipe(gulp.dest(dependencies[src]));
        }
    });

    gulp.task('app:lint', function () {
        return gulp.src(['app/**.js', '!app/app.js'])
            .pipe(jshint())
            .pipe(jshint.reporter('default'));
    });

    gulp.task('app', ['app:dependencies', 'app:lint', 'app:compile']);

    // Watch Files For Changes
    gulp.task('watch', function () {
        gulp.watch('_Scripts/*.js', ['lint', 'scripts']);
        gulp.watch('_Styles/*.scss', ['sass']);
        gulp.watch('app/**', ['app']);
    });

    // starts a livereload webserver.
    gulp.task('webserver', function () {
        gulp.src('./')
            .pipe(webserver({
                livereload: true,
                fallback: 'index.html',
                open: true
            }));
    });

    //development server with live reload
    gulp.task('livewebserver', function () {
        connect.server({
            livereload: true
        });
    });

    //IF SERVER EXISTS
    gulp.task('node', function () {
      nodemon({
        script: 'server.js'
      , ext: 'js html'
      , env: { 'NODE_ENV': 'development' }
      })
    })

    // Default Task
    //gulp.task('default', ['lint', 'sass', 'scripts', 'watch', 'node', 'webserver']);
    gulp.task('default', ['lint', 'sass', 'scripts', 'app', 'node']);
    gulp.task('deploy', ['lint', 'sass', 'scripts', 'app', 'watch', 'livewebserver']);


}());
