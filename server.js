(function () {

    'use strict';

    // import modules
    var express        = require('express'),
        bodyParser     = require('body-parser'),
        methodOverride = require('method-override'),
        routes         = require('./api/routes.js'),
        app            = express();

    // load server config
    require('./api/config/server')(app);

    // initialize the database;
    require('./api/config/db')();

    // set directory for static files
    app.use(express.static(__dirname + '/app'));

    // add routes to the app
    require('./api/routes')(app);

    app.start();

}());
