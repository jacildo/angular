/*global angular */
/*global appServices */

(function () {
    "use strict";

    angular.module('appServices')
        .factory('fakeFactory', fakeFactory);

    fakeFactory.$inject = ['$http', '$q', '$resource'];

    function fakeFactory($http, $q, $resource) {

        var root = 'http://jsonplaceholder.typicode.com';

        return $resource(root + "/posts", {}, {
            query: {method: 'GET', params: {}, isArray: true}
        });

    }

}());

