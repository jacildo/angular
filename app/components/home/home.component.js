/*global angular */
(function () {
    "use strict";

    angular.module('app')
        .controller('homeController', homeController);

    homeController.$inject = ['$scope', '$http'];

    function homeController($scope, $http) {
        $scope.message = "Hello from the other side";
    }
}());

