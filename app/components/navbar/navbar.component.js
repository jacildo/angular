/*global angular */
(function () {

    "use strict";

    angular.module('app')
        .controller('navbarController', navbarController)
        .directive('navbar', function () {
            return {
                restrict: 'E',
                templateUrl: 'components/navbar/navbar.component.html',
                controller: 'navbarController'
            };
        });

    navbarController.$inject = ['$scope', '$http'];

    function navbarController($scope, $http) {
        $scope.message = "Hello from the other side";
    }
}());
