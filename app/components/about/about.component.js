/*global angular */
(function () {
    "use strict";

    angular.module('app')
        .controller('aboutController', aboutController);

    aboutController.$inject = ['$scope', '$http', 'fakeFactory'];

    function aboutController($scope, $http, fakeFactory) {
        $scope.posts = fakeFactory.query();

    }
}());

