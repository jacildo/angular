/*global angular */
/*global appServices */

(function () {
    "use strict";

    angular.module('appServices')
        .factory('blogsService', blogsService);

    blogsService.$inject = ['$http', '$q', '$resource'];

    function blogsService($http, $q, $resource) {

        var root = 'http://jsonplaceholder.typicode.com';

        return $resource(root + "/posts", {}, {
            query: {method: 'GET', params: {}, isArray: true}
        });

    }

}());

