/*global angular */
(function () {
    "use strict";

    angular.module('app')
        .controller('blogsController', blogsController);

    blogsController.$inject = ['$scope', '$http', 'blogsService'];

    function blogsController($scope, $http, blogsService) {
        $scope.posts = blogsService.query();

    }
}());

