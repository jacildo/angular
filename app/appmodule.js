/*global angular */
(function () {
    "use strict";

    angular.module('app', ['ui.bootstrap', 'ngRoute', 'ngResource', 'appServices'])
        .config(config);

    config.$inject = ['$routeProvider', '$locationProvider'];

    function config($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'components/home/home.component.html',
                controller: 'homeController',
                controllerAs: 'panel'
            })
            .when('/about', {
                templateUrl: 'components/about/about.component.html',
                controller: 'aboutController',
                controllerAs: 'panel'
            })
            .when('/blogs', {
                templateUrl: 'components/blogs/blogs.component.html',
                controller: 'blogsController',
                controllerAs: 'panel'
            })
            .otherwise({redirectTo: '/'});

        $locationProvider.html5Mode(true);
    }

    var appServices = angular.module('appServices', ['ngResource']);
}());
